# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Feature 主要功能

#### Styling 基本樣式

- 可以透過顏色選擇器 (color picker) 來選顏色，頁面中的選擇器是網路上引入進來的，Reference 連結: [Jscolor](http://jscolor.com/)
- 線條可以條粗度，範圍為 0 ~ 50 px
- 字體也可以調大小，範圍為 0 ~ 100px
- 可以挑字型提供以下幾種 (有可能不支援)
  - Roboto
  - Heebo
  - Montserrat
  - Roboto Condensed
  - Source Sans Pro
  - Oswald
  - Raleway
  - Dokdo
  - Open Sans Condensed
  - Rubik
  - Nanum Gothic
  - Quicksand
  - Work Sans
  - Inconsolata
  - Dosis
  - Indie Flower
  - Anton

#### Brush 筆刷功能

- Line: 畫線條
- Triangle: 畫出空心的三角形
- Circle: 畫出空心圓形
- **Rainbow**: 做出了彩虹軌跡，當然這個彩虹是只有我幾個比較喜歡的顏色在裡面而已，不過很好看。實作的方法就是一直調 RGB 而已，但做出來的結果卻很好。

#### Shape 形狀功能

- Rectangle: 可以畫出實心矩形
- Triangle: 畫出實心三角形
- Circle: 畫出實心圓形

#### Eraser 橡皮擦

- 清掉畫布上的某些位置，可以透過調 line-width 大小來調整橡皮擦大小

#### Text 文字

- 選擇文字後，點擊畫布會產生一個輸入匡，輸入字後按 Enter 即可畫上文字
- 同時只能產生一個文字匡
- 取消輸入只要點擊畫布以外的地方就可以了

#### Redo / Undo / Refresh

- 往前一個狀態或是往回一個狀態
- 實作的話只是先把當時 canvas 的狀況轉成一串 URI 後，需要時在畫回去 Canvas
- 此機制也有用在話 Shape 的時候，目的是讓圖形在實際畫上去前有伸縮的假象

#### Upload 上傳圖片

- 可以上傳圖片，畫到畫布上
- 圖片過大會被縮放到 Canvas 大小，但不壓縮高度

#### Download 下載圖片

- 點擊即可把當時 canvas 上所有畫的東西儲存起來

