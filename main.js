// Get Main Canvas Object

var canvas = document.getElementById('main-frame');
var ctx = canvas.getContext('2d');

// Get Frame Outer
var frameOuter = document.getElementById('frame-outer-main');

// Start Programming

// Global Variable
// Default Setting
var CANVAS_GLOBAL_X;
var CANVAS_GLOBAL_Y;

function setCanvasSetting() {
  // Update Canvas Global Position.
  canvas.width = frameOuter.offsetWidth;
  canvas.height = frameOuter.offsetHeight;
  CANVAS_GLOBAL_X = canvas.getBoundingClientRect().left;
  CANVAS_GLOBAL_Y = canvas.getBoundingClientRect().top;
}

setCanvasSetting();
// Update Global Configuration on Window Resize
window.onresize = () => {
  setCanvasSetting();
  restoreLastState(isWithoutClearState=true);
}

// Brush Setting
var Mode = "line-like"; // line-like, shape-like, eraser, text
var LineSize = 25; // pixel
var LineStyle = "round";
var FontSize = 50;
var FontStyle = "serif";
var Shape = "line"; // circle, triangle, rectangle, line
var Color = "#000000";

// Shape Type
const LineShapes = ["line", "circle", "triangle", "rainbow"];
const ShapeShapes = ["rectangle", "circle", "triangle"];

// 彩虹變數
var RainbowColor = [0, 0, 0]; // RGB
var RainbowSpeed = 20;

// History
var History = {
  startCursorPos: {
    x: 0,
    y: 0
  },
  lastCursorPos: {
    x: 0,
    y: 0
  },
  canvas: {
    canvasState: [],
    curStateIndex: -1
  }
}

function main() {
  // init 
  frameOuter.style.cursor = "url('./icon/paint-palette.svg'), auto";

  // Adding Brush Functionality

  addMouseEvent();

  // Save state
  saveState();

  generateSelectOptions(Mode);

  // Add Buttom
  addButtomClickHandler();

  // Add Input Event
  addInputEvents();
}

/**
 * 
 * @param {Object} infoPayload
 *    drawX: Integer, x position on canvas
 *    drawY: Integer, y position on canvas
 */
async function draw(infoPayload) {
  // Get All Info
  const { x, y } = infoPayload;

  // Get Variable
  let startX = History.startCursorPos.x;
  let startY = History.startCursorPos.y;

  // Detect Mode First
  if (Mode === 'line-like') {
    if (Shape === "circle" || Shape === "triangle") await restoreLastState(isWithoutClearState=true);
    if (Shape === "line") {
      ctx.lineTo(x, y);
      ctx.stroke();
    } else if (Shape === "circle") {
      drawCircle(x, y, startX, startY);
    } else if (Shape === "triangle") {
      drawTriangle(x, y, startX, startY);
    } else if (Shape === "rainbow") {
      ctx.strokeStyle = `rgb(${RainbowColor[0]}, ${RainbowColor[1]}, ${RainbowColor[2]})`;
      ctx.beginPath();
      ctx.moveTo(History.lastCursorPos.x, History.lastCursorPos.y);
      ctx.lineTo(x, y);
      ctx.stroke();
      ctx.closePath();
      
      // Update Ranbow
      if (RainbowColor[0] <= 255 && RainbowColor[1] == 0 && RainbowColor[2] == 0) RainbowColor[0] += RainbowSpeed;
      if (RainbowColor[0] >= 255 && RainbowColor[2] == 0 && RainbowColor[1] <= 255) RainbowColor[1] += RainbowSpeed;
      if (RainbowColor[0] >= 255 && RainbowColor[1] >= 255 && RainbowColor[2] <= 255) RainbowColor[2] += RainbowSpeed;
      if (RainbowColor[2] >= 255 && RainbowColor[1] >= 255 && RainbowColor[0] > 0) RainbowColor[0] -= RainbowSpeed;
      if (RainbowColor[0] == 0 && RainbowColor[2] >= 255 && RainbowColor[1] > 0) RainbowColor[1] -= RainbowSpeed;
      if (RainbowColor[0] == 0 && RainbowColor[1] == 0 && RainbowColor[2] > 0) RainbowColor[2] -= RainbowSpeed;

      History.lastCursorPos.x = x;
      History.lastCursorPos.y = y;
    }
  } else if (Mode === 'shape-like') {
    // Clear And Draw.
    await restoreLastState(isWithoutClearState=true);

    if (Shape === "rectangle") {
      ctx.fillRect(2 * startX - x, 2 * startY - y, 2 * (x - startX), 2 * (y - startY));
    } else if (Shape === "triangle") {
      drawTriangle(x, y, startX, startY, isFill=true);
    } else if (Shape === "circle") {
      drawCircle(x, y, startX, startY, isFill=true);
    } else {
      console.error(`Shape ${Shape} Is Not Supported.`);
    }
  } else if (Mode === 'eraser') {
    ctx.strokeStyle = "rgb(255, 244, 239)";
    ctx.lineTo(x, y);
    ctx.stroke();
  }
}

function drawCircle(x, y, startX, startY, isFill=false) {
  ctx.beginPath();
  ctx.arc((startX + x) / 2, (startY + y) / 2, Math.abs((startX - x)) / 2, 0, 2 * Math.PI);
  
  // Detect Is Line Or Fill
  if (isFill) ctx.fill();
  else ctx.stroke();

  // Close Path
  ctx.closePath();
}

function drawTriangle(x, y, startX, startY, isFill=false) {
  ctx.beginPath();
  ctx.moveTo((startX + x) / 2, startY);
  ctx.lineTo(x,y);
  ctx.lineTo(startX, y);
  ctx.lineTo((startX + x) / 2, startY);

  // Detect is Line or fill.
  if (isFill) ctx.fill();
  else ctx.stroke();

  // Close Path.
  ctx.closePath();
}

function drawText(text, x, y) {
  ctx.fillText(text, x, y);
}

function restoreLastState(isWithoutClearState=false) {
  return new Promise((res, rej) => {
    // Create Temp Image
    let tempImg = new Image();

    // Update Index
    if (!isWithoutClearState && History.canvas.curStateIndex > 0) History.canvas.curStateIndex -= 1;

    // Load State.
    let lastState = History.canvas.canvasState[History.canvas.curStateIndex];

    // Add Load Implementation
    tempImg.onload = () => {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(tempImg, 0, 0);
      res();
    }
    tempImg.src = lastState;
  })
};

function fowardState() {
  return new Promise((res, rej) => {
    if (History.canvas.curStateIndex + 1 < History.canvas.canvasState.length) {
      // Create Temp Image
      let tempImg = new Image();

      // Update Index
      History.canvas.curStateIndex += 1

      // Load State.
      let lastState = History.canvas.canvasState[History.canvas.curStateIndex];

      // Add Load Implementation
      tempImg.onload = () => {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(tempImg, 0, 0);
        res();
      }
      tempImg.src = lastState;
    }
  })
}

function saveState() {
  // Clear Old State
  if (History.canvas.curStateIndex + 1 !== History.canvas.canvasState.length) {
    History.canvas.canvasState = History.canvas.canvasState.slice(0, History.canvas.curStateIndex + 1);
  }
  // Save Current Canvas
  History.canvas.canvasState.push(canvas.toDataURL());
  History.canvas.curStateIndex += 1;
}

function moveCursorHandler(event) {
  // Prepare Payload For Draw
  let infoPayload = {
    x: event.x - CANVAS_GLOBAL_X,
    y: event.y - CANVAS_GLOBAL_Y
  }
  // Closure Variable Define.
  draw(infoPayload);
}

function beforeDraw(event) {
  // Clear Ranbow Color
  RainbowColor = [0, 0, 0];

  // Initialize Canvas Based On Mode.
  if (Mode === "line-like" || Mode === "eraser") {
    ctx.beginPath();
    ctx.moveTo(event.x - CANVAS_GLOBAL_X, event.y - CANVAS_GLOBAL_Y);
    // Update Last Draw
    History.lastCursorPos.x = event.x - CANVAS_GLOBAL_X;
    History.lastCursorPos.y = event.y - CANVAS_GLOBAL_Y;
  }
}

function afterDraw() {
  // Terminate Canvas Operation.
  if (Mode === "line-like" || Mode === "eraser") {
    ctx.closePath();
  }
}

function loadStyling() {
  // Line Style
  ctx.lineCap = LineStyle;
  ctx.lineWidth = LineSize;
  ctx.lineJoin = LineStyle;

  // General Style
  ctx.strokeStyle = Color;
  ctx.fillStyle = Color;

  // Load Text Style
  ctx.font = `${FontSize}px ${FontStyle}`;
}

function clearTextInput() {
  let inputs = frameOuter.querySelectorAll('input');
  for (let node of inputs) node.remove();
}

function addMouseEvent() {
  // Add Mouse Event
  let colorPick = document.getElementById('color-picker');
  let body = document.getElementsByTagName('body')[0];

  // Add Mouse Down Event
  frameOuter.addEventListener('mousedown', (event) => {
    // Get Event x, y;
    let { x, y } = event;

    // Update History
    History.startCursorPos.x = x - CANVAS_GLOBAL_X;
    History.startCursorPos.y = y - CANVAS_GLOBAL_Y;

    // Load Style
    loadStyling();

    // Detect If Mode is text.
    if (Mode === "text") {
      // Clear Previous Input
      clearTextInput();

      let newInput = document.createElement('input');
      // Set Style
      newInput.style.position = 'absolute';
      newInput.style.left = `${x - frameOuter.getBoundingClientRect().left}px`;
      newInput.style.top = `${y - frameOuter.getBoundingClientRect().top}px`;

      // Add Event
      newInput.addEventListener("keyup", (event) => {
        if (event.keyCode === 13) {
          drawText(event.target.value, x - CANVAS_GLOBAL_X, y - CANVAS_GLOBAL_Y);
          saveState();
          newInput.remove();
        }
      })
      // Disable Mousedown Event Propagation.
      newInput.addEventListener("mousedown", (event) => {
        event.stopPropagation();
      })

      // Disable Mousedown Event Propagation.
      newInput.addEventListener("mouseup", (event) => {
        event.stopPropagation();
      })

      // Add To DOM
      frameOuter.appendChild(newInput);
    } else {
      beforeDraw(event);
      frameOuter.addEventListener('mousemove', moveCursorHandler);
    }
    // Deal with Dependency Problem
    // Color Picker
    if (colorPick.classList.contains('jscolor-active')) {
      colorPick.classList.remove('jscolor-active');
      body.childNodes[body.childNodes.length - 1].remove();
    }

    // Stop Propagation
    event.stopPropagation();
  })

  // Add Mouse Up Event
  frameOuter.addEventListener('mouseup', () => {
    // Out Text Mode
    if (Mode === "text") return;

    // Save State
    saveState();

    afterDraw();
    frameOuter.removeEventListener('mousemove', moveCursorHandler);
  });

  // Add Window Event
  window.addEventListener('mousedown', (event) => {
    clearTextInput();
  });
}
// Add File Functionality
function downloadFile() {
  let link = document.createElement('a');
  link.href = canvas.toDataURL();
  link.download = 'image.png';
  link.click();
  link.remove();
}

function uploadFile() {
  let newInput = document.createElement('input');
  newInput.type = 'file';
  newInput.accept = 'image/*';
  newInput.size = 1;
  newInput.addEventListener('change', () => {
    // Construct Image
    let image = new Image();
    let reader = new FileReader();
    let file = newInput.files[0];
    reader.readAsDataURL(file);
    reader.addEventListener('load', () => {
      image.src = reader.result;
      image.onload = () => {
        if (image.width >= canvas.width) ctx.drawImage(image, 0, 0, canvas.width, canvas.width / image.width * image.height);
        else ctx.drawImage(image, 0, 0);
        image.remove();
        newInput.remove();
        saveState();
      }
    })
  })
  newInput.click();
}

// Select Option Generator
function generateSelectOptions(type) {
  let selector = document.getElementById("shape-selector");
  
  // Clear all option
  for(let i = selector.options.length; i >= 0; i--) {
    selector.remove(i);
  }
  if (type === "line-like") {
    for(let o of LineShapes) {
      let option = document.createElement('option');
      option.text = o;
      if (o === Shape) option.selected = "selected";
      selector.add(option);
    }
  } else if (type === "shape-like") {
    for(let o of ShapeShapes) {
      let option = document.createElement('option');
      option.text = o;
      if (o === Shape) option.selected = "selected";
      selector.add(option);
    }
  }
}

// Input Class
function updateWidgetClass(id) {
  let widgets = document.getElementById('widget-container').querySelectorAll('div');
  
  // Update List
  for (let node of widgets) {
    if (node.id === id) node.classList.add('widget-in-active');
    else node.classList.remove('widget-in-active');
  }
}

// Buttom Functionality
function addButtomClickHandler() {
  document.getElementById("widget-download").addEventListener('click', (event) => {
    downloadFile();
  });
  document.getElementById("widget-upload").addEventListener('click', (event) => {
    uploadFile();
  });
  document.getElementById("widget-undo").addEventListener('click', (event) => {
    restoreLastState();
  })
  document.getElementById("widget-redo").addEventListener('click', (event) => {
    fowardState();
  })
  document.getElementById("widget-refresh").addEventListener('click', (event) => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    saveState();
  })
  document.getElementById("line-like-brush").addEventListener('click', (event) => {
    Mode = "line-like";
    // Detect Shape is valid
    Shape = "line";
    generateSelectOptions(Mode);
    updateWidgetClass('line-like-brush');

    // Update Cursor
    frameOuter.style.cursor = "url('./icon/paint-palette.svg'), auto";
  });
  document.getElementById("shape-like-brush").addEventListener('click', (event) => {
    Mode = "shape-like";
    // Detect Shape is valid
    Shape = "rectangle";
    generateSelectOptions(Mode);
    updateWidgetClass('shape-like-brush');

    // Update Cursor
    frameOuter.style.cursor = "url('./icon/spray.svg'), auto";
  });
  document.getElementById("eraser").addEventListener('click', (event) => {
    Mode = "eraser";
    updateWidgetClass('eraser');

    // Update Cursor
    frameOuter.style.cursor = "url('./icon/eraserCursor.svg'), auto";
  });
  document.getElementById("text").addEventListener('click', (event) => {
    Mode = "text";
    updateWidgetClass('text');

    // Update Cursor
    frameOuter.style.cursor = "url('./icon/toys.svg'), auto";
  })
}

// Add Input Event
function addInputEvents() {
  document.getElementById("color-picker").addEventListener('change', (event) => {
    Color = `#${event.target.value}`;
  })
  document.getElementById("shape-selector").addEventListener('change', (event) => {
    Shape = event.target.value;
  })
  document.getElementById("line-width-picker").addEventListener('change', (event) => {
    LineSize = Math.floor(event.target.value / 2);
  })
  document.getElementById("font-size-picker").addEventListener('change', (event) => {
    FontSize = event.target.value;
  })
  document.getElementById("font-type-picker").addEventListener('change', (event) => {
    FontStyle = event.target.value;
  })
}

// Call Main Function
main();